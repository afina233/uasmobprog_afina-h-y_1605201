package cs.upi.edu.uas_1605201_afina_hy.uasmobprog3.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class SensorModel implements Parcelable {
    public int id;
    public String posisi;
    public String waktu;
    public String latitude;
    public String longitude;

    public String getNotif() {
        return notif;
    }

    public void setNotif(String notif) {
        this.notif = notif;
    }

    public String notif;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getWaktu() {

        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getPosisi() {
        return posisi;
    }

    public void setPosisi(String posisi) {
        this.posisi = posisi;
    }

    public SensorModel() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.posisi);
        dest.writeString(this.waktu);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
    }

    protected SensorModel(Parcel in) {
        this.id = in.readInt();
        this.posisi = in.readString();
        this.waktu = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
    }

    public static final Creator<SensorModel> CREATOR = new Creator<SensorModel>() {
        @Override
        public SensorModel createFromParcel(Parcel source) {
            return new SensorModel(source);
        }

        @Override
        public SensorModel[] newArray(int size) {
            return new SensorModel[size];
        }
    };
}
