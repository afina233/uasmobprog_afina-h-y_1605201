package cs.upi.edu.uas_1605201_afina_hy.uasmobprog3;

//NAMA: AFINA HADAINA YUDIANITA
//NIM: 1605201
// UAS MOBILE PROGRAMMING

import android.Manifest;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import cs.upi.edu.uas_1605201_afina_hy.uasmobprog3.DB.SensorDB;
import cs.upi.edu.uas_1605201_afina_hy.uasmobprog3.Model.SensorModel;

public class MainActivity extends AppCompatActivity implements SensorEventListener,View.OnClickListener, GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private SensorManager sm;
    private Sensor senAccel;
    private TextView tvHasil, tvWaktu;
    private Button clear,save,load;
    private Date currentTime;
    private String lat, longi;

    private static final int MY_PERMISSION_REQUEST = 99;
    GoogleApiClient mGoogleApiClient​;
    Location mLastLocation​;
    TextView mLatText​;
    TextView mLongText​;



    @Override
    protected void onResume() {
        super.onResume();

        sm.registerListener(this, senAccel, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sm.unregisterListener(this);
    }

    CustomListAdapter adapter;
    private ArrayList<SensorModel> data;
    private RecyclerView recV;
    private SensorDB dbSensor;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLatText​ = (TextView)findViewById(R.id.tvLat);
        mLongText​ = (TextView)findViewById(R.id.tvLong);
        buildGoogleApiClient();
        createLocationRequest();


        sensorDetect();
        clear = (Button) findViewById(R.id.clear);
        save = (Button) findViewById(R.id.save);
        load = (Button) findViewById(R.id.load);

        clear.setOnClickListener(this);
        save.setOnClickListener(this);
        load.setOnClickListener(this);


        tvHasil = (TextView) findViewById(R.id.tv_hasil);
        tvWaktu = (TextView)findViewById(R.id.tv_waktu);

        recV = (RecyclerView)findViewById(R.id.rv_posisi);

        recV.setHasFixedSize(true);

        data = new ArrayList<>();

        // membuka database dan menampilkan datanya di reyclerview
        dbSensor = new SensorDB(getApplicationContext());
        dbSensor.open();
        dbSensor.deleteAllSensor();
        dbSensor.close();


        loadData();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == MY_PERMISSION_REQUEST){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                ambilLokasi();
            }else{
                AlertDialog ad = new AlertDialog.Builder(this).create();
                ad.setMessage("Tidak mendapat ijin, tidak dapat mengambil lokasi");
                ad.show();
            }
            return;
        }
    }

    protected synchronized void buildGoogleApiClient(){
        mGoogleApiClient​ = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public void ambilLokasi(){
        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_REQUEST);
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient​, mLocationRequest, this);

    }

    LocationRequest mLocationRequest;
    protected void createLocationRequest(){
        mLocationRequest = new LocationRequest();
//        10 detik sekali meminta lokasi (10000ms = 10 detik)
        mLocationRequest.setInterval(10000);
//        tapi tidak boleh lebih cepat dari 5 detik
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void sensorDetect(){
        sm = (SensorManager)    getSystemService(getApplicationContext().SENSOR_SERVICE);
        senAccel = sm.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        if (senAccel != null){
            // ada sensor accelerometer!
        }
        else {
            // gagal, tidak ada sensor accelerometer.
            Toast.makeText(this,"Tidak ada sensor", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    protected void onStart(){
        super.onStart();
        mGoogleApiClient​.connect();
    }

    @Override
    protected void onStop(){
        mGoogleApiClient​.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        ambilLokasi();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onLocationChanged(Location location) {

//        mLatText​.setText("Latitude: " + String.valueOf(location.getLatitude()));
//        mLongText​.setText("Longitude: " + String.valueOf(location.getLongitude()));


        lat = "Latitude: " + String.valueOf(location.getLatitude());
        longi = "Longitude: " + String.valueOf(location.getLongitude());

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        double ax=0,ay=0,az=0;
        // menangkap perubahan nilai sensor
        if (sensorEvent.sensor.getType()==Sensor.TYPE_LINEAR_ACCELERATION) {
            ax=sensorEvent.values[0];
            ay=sensorEvent.values[1];
            az=sensorEvent.values[2];

            // dalam pengereman mendadak, yang digunakan hanya koordinat Z saja
        }

        // data pengereman melihat dari batas minimum sensor yang cukup besar yaitu 6 dan -6
        SensorModel d = new SensorModel();
//

        if(az>6 || az<-6){
            // ketika pengereman mendadak terdeteksi
            currentTime = Calendar.getInstance().getTime();

            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");

            SimpleDateFormat dt = new SimpleDateFormat("HH:mm:ss");
            String formattedTime = dt.format(currentTime);
            String formattedDate = df.format(currentTime);

            d.posisi = "Angkat-turun";
            d.waktu = formattedDate + " / " + formattedTime;
            d.notif = "Pengereman mendadak!!";
            d.longitude = longi;
            d.latitude = lat;
            data.add(d);
            adapter.notifyDataSetChanged();

        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {


    }

    public void loadData(){

        recV.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CustomListAdapter(this);
        adapter.setListSensor(data);
        recV.setAdapter(adapter);
    }

    @Override
    protected void onDestroy(){
        // menutup database
        dbSensor.close();
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.clear){
            data.clear();
            adapter.notifyDataSetChanged();
        }else if(v.getId() == R.id.save){
            dbSensor.open();
            for (SensorModel m :data) {
                dbSensor.insertSensor(m);
            }
            Toast.makeText(this,"Saved", Toast.LENGTH_SHORT).show();

            dbSensor.close();

        }else if(v.getId() == R.id.load){

            dbSensor.open();
            data = dbSensor.getAllSensor();
            dbSensor.close();
            loadData();
            if(data.size()>0){

                Toast.makeText(this,"Loaded", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this,"Empty", Toast.LENGTH_SHORT).show();

            }
        }
    }
}
