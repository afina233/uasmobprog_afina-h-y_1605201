package cs.upi.edu.uas_1605201_afina_hy.uasmobprog3;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import cs.upi.edu.uas_1605201_afina_hy.uasmobprog3.Model.SensorModel;

public class CustomListAdapter extends RecyclerView.Adapter<CustomListAdapter.CategoryViewHolder> {
    private Context context;
    ArrayList<SensorModel> getListSensor() {
        return listSensor;
    }

    private ArrayList<SensorModel> listSensor;
    CustomListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_posisi, parent, false);
        return new CategoryViewHolder(itemRow);
    }
    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
//        holder.tvPosisi.setText(getListSensor().get(position).getPosisi());
        holder.tvWaktu.setText(getListSensor().get(position).getWaktu());
        holder.tvLat.setText(getListSensor().get(position).getLatitude());
        holder.tvLong.setText(getListSensor().get(position).getLongitude());
        holder.tvNotif.setText(getListSensor().get(position).getNotif());


    }
    @Override
    public int getItemCount() {
        return getListSensor().size();
    }

    public void setListSensor(ArrayList<SensorModel> listSensor) {
        this.listSensor = listSensor;
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder{
        TextView tvWaktu;
        TextView tvLat, tvLong, tvNotif;
        CategoryViewHolder(View itemView) {
            super(itemView);
            tvLat = (TextView)itemView.findViewById(R.id.tvLat);
            tvLong = (TextView)itemView.findViewById(R.id.tvLong);
            tvWaktu = (TextView)itemView.findViewById(R.id.tv_waktu);
            tvNotif = (TextView)itemView.findViewById(R.id.tv_hasil);
        }
    }
}
