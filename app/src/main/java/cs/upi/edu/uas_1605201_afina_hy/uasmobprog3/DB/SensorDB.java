package cs.upi.edu.uas_1605201_afina_hy.uasmobprog3.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import cs.upi.edu.uas_1605201_afina_hy.uasmobprog3.Helper.OpenHelper;
import cs.upi.edu.uas_1605201_afina_hy.uasmobprog3.Model.SensorModel;

public class SensorDB {

    private SQLiteDatabase db;
    private final OpenHelper dbHelper;

    public SensorDB(Context c){
        dbHelper = new OpenHelper(c);
    }

    public void open(){
        db  = dbHelper.getWritableDatabase();
    }

    public void close(){
        db.close();
    }


    public boolean insertSensor(SensorModel sensor) {
        ContentValues newValues = new ContentValues();
        newValues.put("POSISI", sensor.posisi);
        newValues.put("WAKTU", sensor.waktu);
        newValues.put("LATITUDE", sensor.latitude);
        newValues.put("LONGITUDE", sensor.longitude);
        newValues.put("NOTIF", sensor.notif);

        try{
            db.insert("SENSOR", null, newValues);
            return true;
        }catch (SQLException ex){
            throw ex;
        }

    }

    public ArrayList<SensorModel> getAllSensor() {
        Cursor cur = null;
        ArrayList<SensorModel> out = new ArrayList<>();
        cur = db.rawQuery("SELECT * FROM Sensor", null);

        if (cur.moveToFirst()) {
            do {
                SensorModel s = new SensorModel();
                s.id = Integer.valueOf(cur.getString(0));
                s.posisi = cur.getString(1);
                s.waktu = cur.getString(2);
                s.latitude = cur.getString(3);
                s.longitude = cur.getString(4);
                s.notif = cur.getString(5);
                out.add(s);
            } while (cur.moveToNext());
        }
        cur.close();
        return out;
    }


    public boolean deleteAllSensor(){
        try {

            db.execSQL("DELETE FROM SENSOR");
            return true;
        }catch (SQLException ex){
            return false;
        }
    }
}
